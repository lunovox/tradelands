local oldfunc = default.can_interact_with_node
local newfunc = function(player, pos)
	if player and player:is_player() then
		if minetest.check_player_privs(player, "protection_bypass") then
			return true
		end
	else
		return false
	end

	local meta = minetest.get_meta(pos)
	local owner = meta:get_string("owner")
	local namePlayer = player:get_player_name()
	if not owner or owner == "" or owner == namePlayer
		or (
			minetest.get_modpath("tradelands")
			and modTradeLands~=nil
			and modTradeLands.getOwnerName(pos)~=""
			and modTradeLands.canInteract(pos, namePlayer)
		)
		or (
			minetest.get_modpath("landrush")
			and landrush~=nil
			and landrush.get_owner(pos)~=nil
			and landrush.can_interact(pos, namePlayer)
		)
	then
		return true
	end

	-- Is player wielding the right key?
	local item = player:get_wielded_item()
	if minetest.get_item_group(item:get_name(), "key") == 1 then
		local key_meta = item:get_meta()

		if key_meta:get_string("secret") == "" then
			local key_oldmeta = item:get_metadata()
			if key_oldmeta == "" or not minetest.parse_json(key_oldmeta) then
				return false
			end

			key_meta:set_string("secret", minetest.parse_json(key_oldmeta).secret)
			item:set_metadata("")
		end

		return meta:get_string("key_lock_secret") == key_meta:get_string("secret")
	end

	return false
end

default.can_interact_with_node = newfunc