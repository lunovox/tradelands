# TRANSLATES

Update: 2024-03-06
version of Readme: 1.7

-----

To generate file [template.pot], did use terminal command:

```bash
cd tradelands
xgettext -n *.lua -L Lua --force-po --keyword=modTradeLands.translate  --from-code=UTF-8 -o ./locale/template.pot

```
-----

### Generate file '.po' per [template.pot] file:

To translate [template.pot] to your language in '.po' file:

* Locales used: ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk

Sintaxe:

```bash
$ msginit --no-translator --no-wrap --locale=$LANG.UTF-8 --output-file=$LANG.po --input=$POT
```

* Example to brazilian portuguese language: [locale/pt_BR.po]

Example:

```bash
msginit --no-translator --no-wrap --locale=pt_BR.UTF-8 --output-file=./locale/pt_BR.po --input=./locale/template.pot
```

-----

### To Update ".po" file from a [template.pot] file.

Sintaxe:

```bash
$ msgmerge --sort-output --no-wrap --update --backup=off $LANG.po $POT
```

* Example to brazilian portuguese language: [locale/pt_BR.po]

Example:

```bash
msgmerge --sort-output --no-wrap --update --backup=off ./locale/pt_BR.po ./locale/template.pot
```

-----

To translate [template.pot] to your language use GUI App [PoEdit].:

### Install the PoEdit:

```bash
sudo apt-get install poedit
```


-----

### Convert '.po' file to '.tr' file.

Translate Sample: [locale/tradelands.pt_BR.tr]
```bash
cd ./locale/
lua5.3 po2tr.lua "tradelands" "pt_BR.po"
mv "pt_BR.tr" "tradelands.pt_BR.tr"
```

-----


### Exemple of enable the brazilian portuguese language in minetest:

Translate Sample: `locale/tradelands.pt_BR.tr`

To enable the translate to brazilian portuguese language, write "language = pt_BR" in file "minetest.conf". Or write the command ```/set -n language pt_BR``` in game chat, and run again the minetest game.

-----

### PLEASE SUBMIT YOUR NEW TRANSLATION TO THE DEVELOPERS OF THIS MOD THROUGH THE GIT PROJECT BELOW:

* `https://gitlab.com/lunovox/tradelands`

----

> See more: 
* https://forum.minetest.net/viewtopic.php?f=47&t=21974
* https://github.com/minetest/minetest/issues/8158
* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
* https://gitlab.com/4w/xtend/-/blob/master/xtend_default/tools/convert_po_file_to_tr_file/convert_po_file_to_tr_file.lua

[locale/pt_BR.po]:https://gitlab.com/lunovox/tradelands/-/raw/master/locale/pt_BR.po
[locale/tradelands.pt_BR.tr]:https://gitlab.com/lunovox/tradelands/-/raw/master/locale/tradelands.pt_BR.tr
[PoEdit]:https://poedit.net
[template.pot]:https://gitlab.com/lunovox/tradelands/-/raw/master/locale/template.pot

