modTradeLands.getDetachedInventory = function() --criar um inventario desatachado de nome 'detached:charter'
	local newInv = minetest.create_detached_inventory("charter", { --trunk
		allow_move = function(inv, from_list, from_index, to_list, to_index, count, player) 
			if from_list=="listPrice" then return 0 end
			return count
		end,
		allow_put = function(inv, listname, index, stack, player) 
			if listname=="listPrice" then return 0 end
			return stack:get_count()
		end,
		allow_take = function(inv, listname, index, stack, player) 
			if listname=="listPrice" then return 0 end
			return stack:get_count()
		end,

		on_move = function(inv, from_list, from_index, to_list, to_index, count, player) end,
		on_put = function(inv, listname, index, stack, player) end,
		on_take = function(inv, listname, index, stack, player) end,
		
	})
	newInv:set_size("listPrice", 2*2)
	newInv:set_size("listPay", 2*2)
	modTradeLands.price={ --Items that the player must pay to protect the land. (Maximum of 4 item types)
   	--"default:gold_ingot 3", "default:steel_ingot 3"
   	--"minertrade:minermoney 3", "minertrade:minercoin 3" --If you want the mod minertrade do download in: https://github.com/Lunovox/minertrade
   	"currency:minegeld_100 1"
   }
	local price = modTradeLands.price
	if #price>=1 then
		for i=1,#price do
			newInv:add_item("listPrice", ItemStack(price[i]))	
		end
	end
	
	return newInv
end

modTradeLands.getFormMain = function(pos, playername)
	local landname = modTradeLands.getLandName(pos) -- Area selecionada
	local ifPermanentValidate = modTradeLands.getPermanentValidate(pos)
	local pnlLeft = 2
	local pnlTop = 0.35
	local pnlWidth = 6 --Defsult:4
	local pnlHeight = 1.5

	local formspec = ""
	--.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	.."background[0,0.50;2.0,2.25;icon_charter.png]"
	
	
	--pnlTop=pnlTop+0.75
	formspec=formspec.."label["..(pnlLeft+0.5)..","..(pnlTop-0.5).." ;"..minetest.formspec_escape(modTradeLands.translate("LAND PERMISSION")).."]"
	
	
	--pnlTop=pnlTop+0.75
	local btnNewTop = pnlTop+0.25
	local btnPanel = "button_exit["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnShowLand;"..minetest.formspec_escape(modTradeLands.translate("Show Protectable Territory")).."]"
	
	
	if not ifPermanentValidate then
		if playername~=nil 
		   and (
		      minetest.get_player_privs(playername).mayor 
		      or (minetest.global_exists("modEUrn") and modEUrn.getPresidentName()==playername)
		   ) 
		then
			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnPublicProtect;"..minetest.formspec_escape(modTradeLands.translate("Public Land Protection")).."]"
		end
		
		--[[  ]]
		if modMinerTrade ~= nil and modMinerTrade.isExistAcount("THE GOVERNMENT") then
			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnPayForm2;"..minetest.formspec_escape(modTradeLands.translate("Bank's Land Protection")).."]"
		else
			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnPayForm;"..minetest.formspec_escape(modTradeLands.translate("Citizen's Land Protection")).."]"		
		end
		--]]
	end
	
	btnNewTop=btnNewTop+0.75
	btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnListLands;"..minetest.formspec_escape(modTradeLands.translate("Your Land List")).."]"		

	--[[  ]]
	local ownername = modTradeLands.getOwnerName(pos)
	if ownername~="" then
		if ownername==playername 
		   or minetest.get_player_privs(playername).mayor 
		   or (minetest.global_exists("modEUrn") and modEUrn.getPresidentName()==playername)
		then
			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnTransferOwnership;"..minetest.formspec_escape(minetest.colorize("#888", modTradeLands.translate("Transfer Ownership"))).."]"		
		
			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnGuestsForm;"..minetest.formspec_escape(modTradeLands.translate("Guest List")).."]"

			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnConfigForm;"..minetest.formspec_escape(modTradeLands.translate("Protected Land Settings")).."]"

			btnNewTop=btnNewTop+0.75
			btnPanel=btnPanel.."button["..(pnlLeft+0.35)..","..(btnNewTop)..";"..(pnlWidth - 0.5)..",1;btnGiveUpForm;"..minetest.formspec_escape(modTradeLands.translate("Leave Ground")).."]"

			if 
			   minetest.get_player_privs(playername).mayor 
			   or (minetest.global_exists("modEUrn") and modEUrn.getPresidentName()==playername)
			then
				btnNewTop=btnNewTop+0.75
				btnPanel=btnPanel.."checkbox["..(pnlLeft+0.35)..","..(btnNewTop)..";chkPermanentValidate;"..minetest.formspec_escape(modTradeLands.translate("Permanent Protection"))..";"..tostring(ifPermanentValidate).."]"
			end
		end
	end
	--]]
	
	pnlHeight = btnNewTop + 0.75
	formspec=formspec.."box["..(pnlLeft)..","..(pnlTop)..";"..(pnlWidth)..","..(pnlHeight)..";#00000088]"
	btnPanel = btnPanel .."label["..(pnlLeft+0.35)..","..(pnlTop+pnlHeight)..";"
	   ..minetest.formspec_escape(
	      modTradeLands.translate("Land")..": "..landname
	      --.." ("..minetest.hash_node_position(pos)..")"
	   )
	.."]"
	formspec=formspec..btnPanel

	local frmWidth = pnlLeft + pnlWidth + 0.5
	local frmHeight = pnlTop + pnlHeight + 0.25
	return "size["..frmWidth..","..frmHeight.."]"..formspec
end

modTradeLands.getFormGiveUpLand = function(pos, playername)
	local formspec = "size[7.5,2.25]"
	--.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	.."box[-0.0,-0.1;7.25,0.6;#000088]"
	.."label[1.5,0;"..minetest.formspec_escape(modTradeLands.translate("LEAVING TERRITORY")).."]"
	.."label[0,0.75;"..minetest.formspec_escape(modTradeLands.translate("Do you really want to unprotect this land?")).."]"
	.."button[0.75,1.5;3,1;btnGiveUpYes;"..minetest.formspec_escape(modTradeLands.translate("Unprotect")).."]"
	.."button[3.75,1.5;3,1;btnGiveUpNot;"..minetest.formspec_escape(modTradeLands.translate("Cancel")).."]"
	return formspec
end

modTradeLands.getFormSpecGuests = function(pos, playername)
	local listGuests = modTradeLands.getGuestsTextList(pos)
	local formspec = "size[6,6.5]"
	--.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots

	   .."label[0,0;"..minetest.formspec_escape(modTradeLands.translate("Land Guests")..":").."]"
	.."textlist[0,0.5;5.85,2;selGuest;"..listGuests..";0;false]"
	  .."button[0,2.60;6.00,1;btnDelGuest;"..minetest.formspec_escape(modTradeLands.translate("Remove Guest")).."]"
	--.."pwdfield[0.29,4.25;4,1;txtNewGuest;Nome do Convidado]"

	 .."field[0.29,4.25;6.00,1;txtNewGuest;"..minetest.formspec_escape(modTradeLands.translate("Guest Name")..":")..";]"
	.."button[0,4.75;6.00,1;btnNewGuest;"..minetest.formspec_escape(modTradeLands.translate("Add Guest")).."]"

			  .."box[0.00,5.65;5.75,1.00;#00000088]"
		  .."button[0.50,5.75;2.50,1.00;btnShowMain;"..minetest.formspec_escape(modTradeLands.translate("Back")).."]"
	.."button_exit[3.00,5.75;2.50,1.00;;"..minetest.formspec_escape(modTradeLands.translate("Close")).."]"
	return formspec
end


modTradeLands.getFormSpecListLands = function(playername, pos)
	local formspec = "size[15,7.00]"
	if type(playername)=="string" and playername~="" then
		formspec = formspec
		.."box[-0.0,-0.1;14.75,0.6;#000088]"
		.."label[0.25,0;"..minetest.formspec_escape(modTradeLands.translate("PROTECTED LANDS OF '@1'", playername)..":").."]"
		local	itemList = "#008888"
			..", "..minetest.formspec_escape(modTradeLands.translate("N°"))
			.." , "..minetest.formspec_escape(modTradeLands.translate("LANDNAME"))
			.." , "..minetest.formspec_escape(modTradeLands.translate("LANDTYPE"))
			.." , "..minetest.formspec_escape(modTradeLands.translate("VALIDATE"))
		local myLands = modTradeLands.getLandsByOwnername(playername)
		local selItem = 0
		if type(myLands)=="table" and table.getn(myLands) >= 1 then
			for i, landprop in ipairs(myLands) do
				local landname = myLands[i].landname
				local isPermanet = myLands[i].permanent_validate or false
				local isAutoReprotection = type(myLands[i].auto_reprotection)~="boolean" or myLands[i].auto_reprotection
				local validate = myLands[i].validate or 0
				local txtValidate = os.date("%Y-%m-%d %Hh:%Mm:%Ss", validate)
				local now = os.time()
				local rest = validate - now
				local dayRest = rest / (60*60*24)
				local typeLand = "Normal"
				
				local lineColor = "#FFF"
				if isPermanet then
					lineColor = "#888"
					typeLand = modTradeLands.translate("Permanent")
					txtValidate = modTradeLands.translate("No Validate")
				else
					if isAutoReprotection then
						typeLand = modTradeLands.translate("Reprotector")
					end
					
					if dayRest > 3 then
						txtValidate = txtValidate .." (".. modTradeLands.translate("Rest @1 days", ("%02d"):format(math.ceil(dayRest)))..")"
					elseif dayRest <= 0 then
						lineColor = "#FF0000"
						txtValidate = txtValidate .." (".. modTradeLands.translate("Expired")..")"
						typeLand = modTradeLands.translate("Unprotected")
					elseif dayRest <= 3 then
						lineColor = "#FFFF00"
						txtValidate = txtValidate .." (".. modTradeLands.translate("Rest @1 days", ("%02d"):format(math.ceil(dayRest)))..")"
						typeLand = modTradeLands.translate("Almost Unprotected")
					end
				end
				
				local plusLandName = ""
				if type(pos)=="table"
					and type(pos.x)=="number"
					and type(pos.y)=="number"
					and type(pos.z)=="number"
				then
					if landname == modTradeLands.getLandName(pos) then
						plusLandName = plusLandName .." ("..modTradeLands.translate("here")..")"
						selItem = i + 1
					end
				end
				itemList = itemList
				..","..lineColor
				..", "..minetest.formspec_escape(("%03d"):format(i))
				.." , "..minetest.formspec_escape(landname..plusLandName)
				.." , "..minetest.formspec_escape(typeLand)
				.." , "..minetest.formspec_escape(txtValidate)
			end --Final of: for i, landprop in ipairs(myLands) do
			
			formspec = formspec
				  .."button[0.00,6.75;2.50,0.50;btnSelLandRenovate;"..minetest.formspec_escape(minetest.colorize("#888", modTradeLands.translate("Renovate"))).."]"
				  .."button[2.50,6.75;2.50,0.50;btnSelLandTransfer;"..minetest.formspec_escape(minetest.colorize("#888", modTradeLands.translate("Transfer"))).."]"
				  .."button[5.00,6.75;2.50,0.50;btnSelLandAbandon;"..minetest.formspec_escape(minetest.colorize("#888", modTradeLands.translate("Abandon"))).."]"
			
			if minetest.get_player_privs(playername).teleport then
			   formspec = formspec
				  .."button[7.50,6.75;2.50,0.50;btnSelLandTeleport;"..minetest.formspec_escape(minetest.colorize("#888", modTradeLands.translate("Teleport"))).."]"
			end
			
			formspec = formspec
				  .."button[10.75,6.75;2.00,0.50;btnShowMain;"..minetest.formspec_escape(modTradeLands.translate("Back")).."]"
			.."button_exit[12.75,6.75;2.00,0.50;;"..minetest.formspec_escape(modTradeLands.translate("Close")).."]"
			
		end --Final of: if type(myLands)=="table" and table.getn(myLands) >= 1 then
		formspec = formspec
		.."         style[lstLands;bgcolor=red;textcolor=yellow;border=true]"
		.."  tablecolumns[color;text;text;text;text]"
		..         "table[0.0,0.75;14.75,5.75;lstLands;"..itemList..";"..selItem.."]"
	end --Final of: if type(playername)=="string" and playername~="" then
	return formspec
end

modTradeLands.getFormSpecConfig = function(pos, playername)
	local ifPermanentValidate = modTradeLands.getPermanentValidate(pos)
	local damage = modTradeLands.getDamageInteract(pos)
	local ifDamageString = modTradeLands.getIfDamageString(pos)
	local ifAlertString = tostring(modTradeLands.getIfAlertOwner(pos))
	local ifAutoReprotection = tostring(modTradeLands.getIfAutoReprotection(pos))
	
	local pvpTypeIndex = modTradeLands.getPvpTypeIndex(pos)
	local pvpStrings = modTradeLands.getPvpStrings()
	
	local formspec = "size[12,5.00]"
	--.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	.."box[-0.0,-0.1;11.75,0.6;#000088]"
	.."label[0.25,0;"..minetest.formspec_escape(modTradeLands.translate("LAND CONFIGURATION")).."]"
	--{"checkbox", x=<X>, y=<Y>, name="<name>", label="<label>", selected=<selected>}
	.."checkbox[0,0.50;chkIfDamage;"..minetest.formspec_escape(modTradeLands.translate("Enable damage by forced interaction."))..";"..ifDamageString.."]"
	.."checkbox[0,1.25;chkIfAlert;"..minetest.formspec_escape(modTradeLands.translate("Alert land owner by forced iteration from other player."))..";"..ifAlertString.."]"
	
	if not ifPermanentValidate then
	   formspec = formspec
	   .."checkbox[0,2.00;chkIfAutoReprotection;"
	   ..minetest.formspec_escape(
	      modTradeLands.translate(
	         "Automatically renew protection if less than @1 days to expire."
	         , ("%02d"):format(modTradeLands.protected_days)
         )
      )..";"..ifAutoReprotection.."]"
	end
   formspec = formspec
	.."label[0,3.12;"..minetest.formspec_escape(modTradeLands.translate("PVP type")..":").."]"
	.."dropdown[0.5,3.75;6,0.25;selPvpType;"..pvpStrings..";"..pvpTypeIndex.."]"
	.."button_exit[5.00,4.50;2,1;btnConfigClose;"..minetest.formspec_escape(modTradeLands.translate("CLOSE")).."]"
	return formspec
end

modTradeLands.getFormSpecPay = function(pos, playername)
	local ownername = modTradeLands.getOwnerName(pos)
	if ownername=="" then 
		ownername=modTradeLands.translate("Nobody")
	end
	local dayRests =  modTradeLands.getDaysRest(pos)
	local strValidate = modTradeLands.translate("Without Maturity")
	local strRest = ""
	if dayRests>0 then 
		strValidate = modTradeLands.getValidateString(modTradeLands.getValidate(pos)) 
		if dayRests > 0 then strRest="("..( modTradeLands.translate("%02d days remain."):format(math.ceil(dayRests)) )..")"	end --format("pi = %.4f", PI)
	end
	local strNewValidate = modTradeLands.getValidateString(modTradeLands.getNewValidate(pos))
	if modTradeLands.protected_days>0 then
		strNewValidate=strNewValidate.." ("..( modTradeLands.translate("Up to %02d days"):format(modTradeLands.protected_days) )..")"
	end
	
	local formspec = "size[9,9]"
	--.."bgcolor[#636D76FF;false]"
	--..default.gui_bg
	--..default.gui_bg_img
	--..default.gui_slots
	.."label[1.5,0;"..minetest.formspec_escape(modTradeLands.translate("LAND PROTECTION WILL")).."]"
	
	--.."vertlabel[0,3;Preço:]"
	.."label[0.5,0.5;"..minetest.formspec_escape(modTradeLands.translate("Cost"))..":]"
	.."list[detached:charter;listPrice;0.5,1;2,2;]"

	.."label[3.5,0.5;"..minetest.formspec_escape(modTradeLands.translate("Payment"))..":]"
	.."list[detached:charter;listPay;3.5,1;2,2;]"


	.."label[0.5,3;"
	   ..minetest.formspec_escape(
	      modTradeLands.translate("Maturity")..": "..strValidate.." "..strRest.."\n"
         ..modTradeLands.translate("Renovation")..": "..strNewValidate
	   )
	.."]"

	.."label[0.5,4;"
      ..minetest.formspec_escape(
         modTradeLands.translate("Current Owner")..": "..ownername.." \n"
         ..modTradeLands.translate("New Owner")..": "..playername
      )
   .." ]"

	--.."button[5.5,1.00;2,1;btnPay;Pagar]"
	.."button_exit[5.5,1.00;2,1;btnPay;"..minetest.formspec_escape(modTradeLands.translate("Pay")).."]"
	.."button_exit[5.5,1.75;2,1;btnCancel;"..minetest.formspec_escape(modTradeLands.translate("Cancel")).."]"
	.."list[current_player;main;0,5;8,4;]"
	return formspec
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname == "frmTradelands" then -- This is your form name
		local playername = player:get_player_name()
		local playerpos = player:get_pos()
		--minetest.chat_send_player(playername, "minetest.register_on_player_receive_fields(player, formname, fields) ==> playername='"..playername.."' formname='"..formname.."' fields="..dump(fields))
		if fields.btnShowMain then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(playerpos, playername))
		elseif fields.btnShowLand then
			minetest.chat_send_player(playername, "[TRADELANDS] "..modTradeLands.translate("Displaying the current terrain size!"))
			modTradeLands.doShowLand(playername) --Mostra o limite do territorio onde o jogador esta.
		elseif fields.btnPayForm then
			if type(modTradeLands.formPlayer)=="nil" then modTradeLands.formPlayer = {} end
			if type(modTradeLands.formPlayer[playername])=="nil" then modTradeLands.formPlayer[playername] = {} end
			modTradeLands.formPlayer[playername].invLandPay = modTradeLands.getDetachedInventory()
			modTradeLands.formPlayer[playername].selPos = playerpos --ATENCAO: O terreno protegido eh onde o jogador esta, e nao onde o jogar apontar.
			modTradeLands.doShowLand(playername) --Mostra o limite do territorio onde o jogador esta.
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecPay(playerpos, playername))
		elseif fields.btnPublicProtect then
			if type(modTradeLands.formPlayer)=="nil" then modTradeLands.formPlayer = {} end
			if type(modTradeLands.formPlayer[playername])=="nil" then modTradeLands.formPlayer[playername] = {} end
			modTradeLands.formPlayer[playername].invLandPay = modTradeLands.getDetachedInventory()
			modTradeLands.formPlayer[playername].selPos = playerpos --ATENCAO: O terreno protegido eh onde o jogador esta, e nao onde o jogar apontar.
			--modTradeLands.doShowLand(playername) --Mostra o limite do territorio onde o jogador esta.
			--minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecPay(playerpos, playername))
			modTradeLands.doPermProtect(playername)
			modTradeLands.setOwnerName(playerpos, "THE GOVERNMENT")
		elseif fields.btnPay then
			modTradeLands.doPay(playername)
		elseif modMinerTrade ~= nil and modTradeLands.getFormPay2 ~= nil and fields.btnPayForm2 then --Will pay with bank account.
			--minetest.chat_send_player(playername, "[TRADELANDS:ERRO] "..modTradeLands.translate("Form of button '%s' are in construction!"):format("fields.btnPayForm2"))
			if type(modTradeLands.formPlayer)=="nil" then modTradeLands.formPlayer = {} end
			if type(modTradeLands.formPlayer[playername])=="nil" then modTradeLands.formPlayer[playername] = {} end
			--modTradeLands.formPlayer[playername].invLandPay = modTradeLands.getDetachedInventory()
			modTradeLands.formPlayer[playername].selPos = playerpos --ATENCAO: O terreno protegido eh onde o jogador esta, e nao onde o jogar apontar.
			--modTradeLands.doShowLand(playername) --Mostra o limite do territorio onde o jogador esta.
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormPay2(playerpos, playername))
		elseif modMinerTrade ~= nil and modTradeLands.doPay2 ~= nil and fields.btnPay2 then
			--minetest.chat_send_player(playername, "[TRADELANDS:ERRO] "..modTradeLands.translate("The '%s' are in construction!"):format("fields.btnPay2"))
			modTradeLands.doPay2(playername)
		elseif fields.btnCancel or fields.quit then
			modTradeLands.giveChange(playername)
		elseif fields.btnGuestsForm then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecGuests(playerpos, playername))
		elseif fields.btnNewGuest then
			if type(fields.txtNewGuest)=="string" and fields.txtNewGuest:trim()~="" then
				modTradeLands.addGuest(playerpos, fields.txtNewGuest:trim())
			else
				minetest.chat_send_player(playername, "[TRADELANDS:ERRO] "..modTradeLands.translate("Enter the 'Guest Name' before pressing the 'Add Guest' button!"))
			end
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecGuests(playerpos, playername))
		elseif fields.selGuest then
			if type(modTradeLands.formPlayer)=="nil" then modTradeLands.formPlayer = {} end
			if type(modTradeLands.formPlayer[playername])=="nil" then modTradeLands.formPlayer[playername] = {} end
			local guests = modTradeLands.getGuests(playerpos)
			local event = minetest.explode_textlist_event(fields.selGuest)
			--minetest.chat_send_player(playername, "event="..dump(event))
			if event.type=="CHG" and event.index>=1 and event.index<=#guests then
				modTradeLands.formPlayer[playername].selGuest = event.index
			end
		elseif fields.btnDelGuest then
			if modTradeLands.formPlayer and modTradeLands.formPlayer[playername] and modTradeLands.formPlayer[playername].selGuest then
				local guests = modTradeLands.getGuests(playerpos)
				local selGuest = modTradeLands.formPlayer[playername].selGuest
				--minetest.chat_send_player(playername, "selGuest="..dump(selGuest).." guests="..dump(guests))
				if type(selGuest)=="number" and selGuest>=1 and selGuest<=#guests then
					modTradeLands.delGuest(playerpos, selGuest)
					minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecGuests(playerpos, playername))
				end
			end
		elseif fields.btnListLands then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecListLands(playername, playerpos))
		elseif fields.btnConfigForm then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormSpecConfig(playerpos, playername))
		elseif type(fields.chkIfDamage)=="string" then
			modTradeLands.setIfDamageInteract(playerpos, (fields.chkIfDamage=="true"))
		elseif type(fields.chkIfAlert)=="string" then
			modTradeLands.setIfAlertOwner(playerpos, fields.chkIfAlert=="true")			
		elseif type(fields.chkIfAutoReprotection)=="string" then
			modTradeLands.setIfAutoReprotection(playerpos, fields.chkIfAutoReprotection=="true")
		elseif type(fields.selPvpType)=="string" then
			modTradeLands.setPvpType(playerpos, fields.selPvpType)
		elseif fields.btnGiveUpForm then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormGiveUpLand(playerpos, playername))
		elseif fields.btnGiveUpYes then
			modTradeLands.doGiveUpLand(playerpos)
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(playerpos, playername))
		elseif fields.btnGiveUpNot then
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(playerpos, playername))
		elseif type(fields.chkPermanentValidate)=="string" then
			modTradeLands.setPermanentValidate(playerpos, (fields.chkPermanentValidate=="true"))
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(playerpos, playername))
		end
	end
end)
	
modTradeLands.giveChange = function(playername) --Recuperar o troco da caixa de pagamento
	if type(modTradeLands.formPlayer)~="nil" 
		and type(modTradeLands.formPlayer[playername])~="nil" 
		and type(modTradeLands.formPlayer[playername].invLandPay)~="nil" 
	then
		local inv = modTradeLands.formPlayer[playername].invLandPay
		local listPay = inv:get_list("listPay")
		if listPay then
			local player = minetest.get_player_by_name(playername)
			if player and player:is_player() then
				for _,item in ipairs(listPay) do
					player:get_inventory():add_item("main",item)
					inv:remove_item("listPay",item)
				end
			end
		end
	end
end

modTradeLands.doPermProtect = function(playername) 
	if type(modTradeLands.formPlayer)~="nil" 
		and type(modTradeLands.formPlayer[playername])~="nil" 
		and type(modTradeLands.formPlayer[playername].selPos)~="nil" 
	then
		local selPos = modTradeLands.formPlayer[playername].selPos
		local restDays = modTradeLands.getDaysRest(selPos)
		if restDays <= modTradeLands.protected_days then --Verifica se o jogador ja tem pago adiantado
			modTradeLands.setOwnerName(selPos, playername)
			modTradeLands.setIfDamageInteract(selPos, true)
			modTradeLands.setPvpType(
				selPos, 
				--modTradeLands.default_pvp --type:normal
				modTradeLands.pvp_types[1] --type: 1 (Nome)
			)
			--modTradeLands.setValidate(selPos, modTradeLands.getNewValidate(selPos))
			modTradeLands.setPermanentValidate(selPos, true)
			--minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(selPos, playername))
			modTradeLands.doShowLand(playername)
			modTradeLands.doSave()
	
			--minetest.chat_send_player(playername, "[TRADELANDS] Parabens! Voce se tornou proprietario deste territorio!")
			minetest.chat_send_all("[TRADELANDS] ".. modTradeLands.translate("The player '%s' protected the field (%s)!"):format(playername, modTradeLands.getLandName(selPos)) )
			--modTradeLands.doSoundProtector()
		else
			minetest.chat_send_player(playername, "[TRADELANDS:AVISO] "..modTradeLands.translate("You can pay an advance license fee only once!"))
		end
	end
end

modTradeLands.doPay = function(playername) --Faz o pagamento do terreno
	if type(modTradeLands.formPlayer)~="nil" 
		and type(modTradeLands.formPlayer[playername])~="nil" 
		and type(modTradeLands.formPlayer[playername].invLandPay)~="nil" 
		and type(modTradeLands.formPlayer[playername].selPos)~="nil" 
	then
		local selPos = modTradeLands.formPlayer[playername].selPos
		local minv = modTradeLands.formPlayer[playername].invLandPay
		local restDays = modTradeLands.getDaysRest(selPos)
		if restDays <= modTradeLands.protected_days then --Verifica se o jogador ja tem pago adiantado
			local ifFaltaItem = false
			local listPrice = minv:get_list("listPrice")
			for i, item in pairs(listPrice) do
				if not minv:contains_item("listPay",item) then
					ifFaltaItem = true
					break
				end
			end
			if not ifFaltaItem then
				for i, item in pairs(listPrice) do
					minv:remove_item("listPay",item)
				end
				modTradeLands.setOwnerName(selPos, playername)
				modTradeLands.setIfDamageInteract(selPos, true)
				modTradeLands.setPvpType(selPos, modTradeLands.default_pvp)
				modTradeLands.setValidate(selPos, modTradeLands.getNewValidate(selPos))
				modTradeLands.doShowLand(playername)
				modTradeLands.doSave()
		
				--minetest.chat_send_player(playername, "[TRADELANDS] Parabens! Voce se tornou proprietario deste territorio!")
				minetest.chat_send_all("[TRADELANDS] ".. modTradeLands.translate("The player '%s' protected the field (%s)!"):format(playername, modTradeLands.getLandName(selPos)) )
				modTradeLands.doSoundProtector()
			else
				minetest.chat_send_player(playername, "[TRADELANDS:AVISO] "..modTradeLands.translate("Make sure you have correctly offered the license fee!"))
			end
		else
			minetest.chat_send_player(playername, "[TRADELANDS:AVISO] "..modTradeLands.translate("You can pay an advance license fee only once!"))
		end
		modTradeLands.giveChange(playername)
	end
end

modTradeLands.doUseCharter = function(user)
	local playername = user:get_player_name()
	local selPos = user:get_pos()
	if selPos.y >= modTradeLands.getMaxDepth() then
		local now = os.time()
		local ownername = modTradeLands.getOwnerName(selPos)
		local restDays = modTradeLands.getDaysRest(selPos)
		if ownername=="" 
		   or playername==ownername 
		   or restDays==0 
		   or minetest.get_player_privs(playername).mayor 
		   or (minetest.global_exists("modEUrn") and modEUrn.getPresidentName()==playername)
		then
			if type(modTradeLands.formPlayer)=="nil" then modTradeLands.formPlayer = {} end
			if type(modTradeLands.formPlayer[playername])=="nil" then modTradeLands.formPlayer[playername] = {} end
			modTradeLands.formPlayer[playername].invLandPay = modTradeLands.getDetachedInventory()
			modTradeLands.formPlayer[playername].selPos = selPos --ATENCAO: O terreno protegido eh onde o jogador esta, e nao onde o jogar apontar.
			minetest.show_formspec(playername, "frmTradelands", modTradeLands.getFormMain(selPos, playername))
		else
			minetest.chat_send_player(
				playername, "[TRADELANDS] "
				..modTradeLands.translate(
					"This lot belongs to '@1' for more @2 days!",
					ownername, 
					string.format("%02d",math.ceil(restDays))
				)
			)
		end
	else
		minetest.chat_send_player(playername, "[TRADELANDS] "..modTradeLands.translate("You can not use the license very deeply!"))
	end
end

minetest.register_craftitem("tradelands:charter", {
	description = modTradeLands.translate("Land Protection Permit")
		.." ("..modTradeLands.areaSize.side.."x"..modTradeLands.areaSize.side..")",
	inventory_image = "icon_charter.png",
	on_use = function(itemstack, player, pointed_thing)
   	modTradeLands.doUseCharter(player)
   	
   	--[[  
   	--SAMPLE OF TEXTAREA COLORIZED
   	local playername = player:get_player_name()
   	minetest.show_formspec(
   		playername, "test:formspec",
			"size[6,4]"..
			(
				"textarea[0.5,1;6,3;;%s, consetetur sadipscing elitr, %s "..
				"ut labore et dolore magna aliquyam erat, sed diam voluptua."..
				"At vero eos et accusam;]"
			):format(
				minetest.colorize("#EE0", "Lorem ipsum dolor sit amet"),
				minetest.colorize("#0DD", "sed diam nonumy eirmod tempor invidunt")
			)
		)
		--]]
   end,
})

minetest.register_craft({
	output = 'tradelands:charter',
	recipe = {
		{"default:paper"	,"default:paper"	,""},
		{"default:paper"	,"default:paper"	,""},
		{"group:dye"		,"default:paper"	,"default:skeleton_key"},
	}
})

minetest.register_alias("charter", "tradelands:charter")
minetest.register_alias(modTradeLands.translate("charter"), "tradelands:charter")
