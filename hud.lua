

modTradeLands.delHUD_v2 = function(player)
   if type(player) ~= "nil" and minetest.is_player(player) then
      local playername = player:get_player_name()
      if type(modTradeLands.hudPlayer)=="table" 
		   and type(modTradeLands.hudPlayer[playername])=="table" 
		   and type(modTradeLands.hudPlayer[playername].hud_id) == "number" 
		then
         local numHudId = modTradeLands.hudPlayer[playername].hud_id
         local tblHudDef = player:hud_get(numHudId)
         if type(tblHudDef) == "table" then
            player:hud_remove(numHudId)
            modTradeLands.hudPlayer[playername] = nil
         end
      end
   end
end

modTradeLands.drawHUD_v2 = function(dtime)
   if type(modTradeLands.hudRestTime) ~= "number" then
      modTradeLands.hudRestTime = 0
   end
   if type(modTradeLands.hudPlayer) ~= "table" then
      modTradeLands.hudPlayer = { }
   end
	modTradeLands.hudRestTime = modTradeLands.hudRestTime + dtime
	if modTradeLands.hudRestTime > 3 then
		modTradeLands.hudRestTime=0
		local players = minetest.get_connected_players()
		for _, player in ipairs(players) do
		   if type(player) ~= "nil" and minetest.is_player(player) then
   		   local playername = player:get_player_name()
   			local playerpos = player:get_pos()
   			local ownername = modTradeLands.getOwnerName(playerpos)
   			local landname = modTradeLands.getLandName(playerpos)
   			
   			local hudColor = 0x88888800 --Invisible text
         	local hudTexto = ""
   			if ownername ~= "" then
      		   --modTradeLands.debug("modTradeLands.drawHUD_v2() : ownername = "..dump(ownername), playername)
      			local ifCanInteract = modTradeLands.canInteract(playerpos, playername)
      			--local numValidate = modTradeLands.getValidate(playerpos)
      			--local numValidateRest = modTradeLands.getValidateRest(playerpos)	
      			local isPermanentValidate = modTradeLands.getPermanentValidate(playerpos)
      			local dayRests =  modTradeLands.getDaysRest(playerpos)
      			hudColor = 0xFFFFFF
      			--hudTexto = "["
      			if not isPermanentValidate then
      		      --modTradeLands.debug("modTradeLands.drawHUD_v2() : isPermanentValidate = "..dump(isPermanentValidate), playername)
      				if dayRests > 0 then 
      					if dayRests <= 5 then
      						hudColor = 0xFFFF00
      						hudTexto = hudTexto..modTradeLands.translate(
      							"Land owned by '@1' for another @2 days.", 
      							ownername, 
      							string.format("%02d", math.ceil(dayRests)) 
      						) 
      					else
      						hudColor = 0xFFFFFF
      						hudTexto = hudTexto..modTradeLands.translate("Land owned by '@1'.", ownername)
      					end
      				else
      					hudColor = 0xFF0000
      					hudTexto = hudTexto..modTradeLands.translate("Land belonged to '@1'.", ownername)
      				end
      			else
      				hudColor = 0x888888
      				hudTexto = hudTexto..modTradeLands.translate("Permanent Land of@n'@1'.", ownername)
      			end
      			--hudTexto = hudTexto.."]"
      		end  --FINAL OF: if ownername~=""then
      		--modTradeLands.debug("modTradeLands.drawHUD_v2() : hudTexto = "..dump(hudTexto), playername)
      	   
   			if type(modTradeLands.hudPlayer[playername]) ~= "table" then
   			   modTradeLands.hudPlayer[playername] = { }
   			end
            --modTradeLands.debug("modTradeLands.drawHUD_v2() : ".."modTradeLands.hudPlayer[playername].hud_id = "..dump(modTradeLands.hudPlayer[playername].hud_id), playername)
   			
   			if type(modTradeLands.hudPlayer[playername].last_landname) ~= "string" 
   			   or modTradeLands.hudPlayer[playername].last_landname ~= landname 
   			   or type(modTradeLands.hudPlayer[playername].last_hudTexto) ~= "string" 
   			   or modTradeLands.hudPlayer[playername].last_hudTexto ~= hudTexto
   			then
   			   --modTradeLands.debug("modTradeLands.drawHUD_v2() : os.time() = "..dump(os.time().." (update frequence)"), playername)
   			   if minetest.is_player(player) then --Check Player Connection
   			      modTradeLands.changeModProtector(player)
   			      modTradeLands.checkAutoReprotection(playerpos)
      			   
      			   local hud_id = ""
         			--modTradeLands.debug("modTradeLands.drawHUD_v2() : player:hud_get_all() = "..dump(player:hud_get_all()), playername)
      			   if type(modTradeLands.hudPlayer)=="table" 
         			   and type(modTradeLands.hudPlayer[playername])=="table" 
         			   and type(modTradeLands.hudPlayer[playername].hud_id) == "number" 
         			then
         			   local numHud = modTradeLands.hudPlayer[playername].hud_id
         			   --modTradeLands.debug("modTradeLands.drawHUD_v2() : myID = "..dump(myID), playername)
         			   local tblHudDef = player:hud_get(numHud)
         			   if type(tblHudDef) == "table"
            			   --and type(player:hud_get_all()) == "table"
            			   --and type(player:hud_get_all()[modTradeLands.hudPlayer[playername].hud_id]) == "table"
         			   then
            			   hud_id = numHud
         			   end
         			end
         			
         			if hud_id ~= "" then
         			   --local hud_list = player:hud_get_all()
         			   --modTradeLands.debug("modTradeLands.drawHUD_v2() : hud_id = "..dump(hud_id), playername)
         			   --modTradeLands.debug("modTradeLands.drawHUD_v2() : type(player:hud_get(hud_id)) = "..dump(type(player:hud_get(hud_id))), playername)
         			   --if type(player:hud_get(hud_id)) == "table" then
         			   --if type(hud_list[hud_id]) == "table" then
            			   --[[  ]]
            			   player:hud_change(hud_id, "text", hudTexto)
            			   --player:hud_change(hud_id, "text2", hudTexto)
            			   player:hud_change(hud_id, "number", hudColor)
            			   --[[  ]]
            			   modTradeLands.hudPlayer[playername].last_landname = landname
            			   modTradeLands.hudPlayer[playername].last_hudTexto = hudTexto
         			   --end
         			else
         			   --HUD ADD
         				modTradeLands.hudPlayer[playername] = {
         					hud_id = player:hud_add({
         						hud_elem_type = "text",
         						name = "Ownerland",
         						number = hudColor,
         						position = {x=0, y=1}, --Padrão: {x=.2, y=.98}
         						offset = {x = 15,	y = -50},
         						text = hudTexto,
         						--text2 = hudTexto,
         						scale = {x=200,y=25}, --Padrão: {x=200,y=25}
         						alignment = {x=1, y=1},
         					}), 
         					last_landname = landname,
         					last_hudTexto = hudTexto,
         				}
      				end
      			end --FINAL OF: if minetest.is_player(player) then
   			end -- FINAL OF: (long test)
   		end --FINAL OF: if type(player) ~= "nil" and minetest.is_player(player) then
		end --FINAL OF: for _, player in ipairs(players) do
	end --FINAL OF: if modTradeLands.hudRestTime > 3 then
end

--[[  ]]
minetest.register_globalstep(function(dtime)
   modTradeLands.drawHUD_v2(dtime)
end) 
--[[  ]]

minetest.register_on_leaveplayer(function(player)
	modTradeLands.delHUD_v2(player)
end)