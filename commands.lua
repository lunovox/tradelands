minetest.register_chatcommand("showland", {
	params = "",
	description = modTradeLands.translate("It highlights the limits of the current protected land."),
	privs = {interact=true},
	func = function(playername, param)
		modTradeLands.doShowLand(playername)
	end,
})

minetest.register_chatcommand("sl", {
	params = "",
	description = modTradeLands.translate("It highlights the limits of the current protected land."),
	privs = {interact=true},
	func = function(playername, param)
		modTradeLands.doShowLand(playername)
	end,
})

minetest.register_chatcommand("showarea", {
	params = "",
	description = modTradeLands.translate("It highlights the limits of the current protected land."),
	privs = {interact=true},
	func = function(playername, param)
		modTradeLands.doShowLand(playername)
	end,
})

minetest.register_chatcommand("sa", {
	params = "",
	description = modTradeLands.translate("It highlights the limits of the current protected land."),
	privs = {interact=true},
	func = function(playername, param)
		modTradeLands.doShowLand(playername)
	end,
})

--[[   
if modMinerTrade and modMinerTrade.showAccountBank and modMinerTrade.showAccountBank.inAtm then
	minetest.register_chatcommand("cityhall", {
		params = "",
		description = modTradeLands.translate("Access the government account bank."),
		privs = {mayor=true},
		func = function(playername, param)
		   local accountname = "City Hall"
		   if not modMinerTrade.isExistAcount(accountname) then
            modMinerTrade.createAcount(accountname)
         end
			modMinerTrade.showAccountBank.inAtm(playername, accountname)
		end,
	})
end
--]]