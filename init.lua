modTradeLands = {
   modname = minetest.get_current_modname(),
   modpath = minetest.get_modpath(minetest.get_current_modname()),
   filedatabase = minetest.get_worldpath().."/tradelands.db", --File that stores the database of protected lands.
}


dofile(modTradeLands.modpath.."/config.lua") -- <== Eh obrigatorio antes de 'api.lua'.
dofile(modTradeLands.modpath.."/translate.lua")  -- <== Tem que ser depois de 'config.lua'.
dofile(modTradeLands.modpath.."/api.lua")
dofile(modTradeLands.modpath.."/hud.lua")
dofile(modTradeLands.modpath.."/minertrade_cityhall.lua")
dofile(modTradeLands.modpath.."/commands.lua")
dofile(modTradeLands.modpath.."/item_charter.lua")
dofile(modTradeLands.modpath.."/item_computing_app.lua")
dofile(modTradeLands.modpath.."/brazutec.lua")
--dofile(modTradeLands.modpath.."/modify_chestlocked.lua")
dofile(modTradeLands.modpath.."/modify_defaultfunction.lua")

minetest.register_entity(
   modTradeLands.get_entity()
)

minetest.register_on_newplayer(function(player)
	modTradeLands.doSave()
end)

minetest.register_on_joinplayer(function(player)
	modTradeLands.doSave()
end)

minetest.register_on_leaveplayer(function(player)
	modTradeLands.doSave()
end)

minetest.register_on_shutdown(function()
	modTradeLands.doSave()
end)

minetest.register_on_punchplayer(function(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)
   modTradeLands.register_on_punchplayer(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)
end)

minetest.register_on_protection_violation(function(pos, playername)
	modTradeLands.register_on_protection_violation(pos, playername)
end)

modTradeLands.doLoad()

minetest.log('action',"["..modTradeLands.modname:upper().."] Carregado!")
