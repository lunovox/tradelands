

if core.global_exists("modComputing") then
   modComputing.add_app("tradelads:btnShowCharter", {
   	icon_name = "btnShowCharter",
   	icon_title = modTradeLands.translate("CHARTER"),
   	icon_descryption = modTradeLands.translate("Land Protection Permit").." ("..modTradeLands.areaSize.side.."x"..modTradeLands.areaSize.side..")",
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_charter.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","["..modTradeLands.modname:upper().."] "
			   ..modTradeLands.translate(
               "Player '@1' is trying to access the '@2' via the '@3'!"
               , playername
               , modTradeLands.translate("Land Protection Permit")
               , "computing app"
            )
         )
			modTradeLands.doUseCharter(player)
   	end,
   })
end