
--[[ --MINERTRADE API
modMinerTrade.isExistAcount = function(playername)
modMinerTrade.doBankSave = function()
modMinerTrade.doBankLoad = function()
modMinerTrade.createAcount = function(playername)
modMinerTrade.getBalance = function(playername)
modMinerTrade.addBalance = function(playername, value)
modMinerTrade.addStatement = function(playername, value, description)
--]]


if modMinerTrade ~= nil then
	modMinerTrade.doBankLoad()
	if not modMinerTrade.isExistAcount("THE GOVERNMENT") then
		modMinerTrade.createAcount("THE GOVERNMENT")
		modMinerTrade.doBankSave()
	end
	
	modTradeLands.getFormPay2 = function(pos, playername, msgDetails)
		local ownername = modTradeLands.getOwnerName(pos)
		if ownername=="" then 
			ownername=modTradeLands.translate("Nobody")
		end
		local dayRests =  modTradeLands.getDaysRest(pos)
		local strValidate = modTradeLands.translate("Without Maturity")
		
		local strRest = ""
		if dayRests>0 then 
			strValidate = modTradeLands.getValidateString(modTradeLands.getValidate(pos)) 
			if dayRests > 0 then 
				strRest="("
				..modTradeLands.translate(
				   "@1 days remain.", 
				   ("%02d"):format(math.ceil(dayRests))
				)
				..")"	
			end --format("pi = %.4f", PI)
		end
		
		local strNewValidate = modTradeLands.getValidateString(modTradeLands.getNewValidate(pos))
		if modTradeLands.protected_days>0 then
			strNewValidate=strNewValidate
			.." ("
			..modTradeLands.translate(
			   "Add @1 days.", 
			   ("%02d"):format(modTradeLands.protected_days)
			)
			..")"
		end
		
		if msgDetails == nil or msgDetails == "" then
			msgDetails = core.colorize("#FFFFFF", modTradeLands.translate("Press the 'Pay' button to use the minercash in your bank account to pay for land protection!"))
		end

		if modMinerTrade ~= "" then
			local msgBalance = ""
			if modMinerTrade.isExistAcount(playername) then
				msgBalance = modTradeLands.translate(
				   "You have @1 minercash.",
				   ("%02d"):format(modMinerTrade.getBalance(playername))
				)
			else
				msgBalance = modTradeLands.translate("Player '@1' is not an account holder of this bank.", playername)
				--.."\n "..strValidate.." "..strRest
			end
			local msgPriceOfProtection = modTradeLands.translate(
			   "It costs @1 minercash to protect the land for @2 real days.",
			   ("%02d"):format(modTradeLands.minercash_price),
				("%02d"):format(modTradeLands.protected_days)
			)
			--.."\n "..strNewValidate

			local formspec = "size[10.25,11]"
			--.."bgcolor[#636D76FF;false]"
			.."bgcolor[#000000;true]"
			--..default.gui_bg
			--..default.gui_bg_img
			--..default.gui_slots
			
			.."background[6.50,0.50;3.0,3.0;icon_charter.png]"
			..       "box[0.25,0.25;9.5,10.5;#00000088]"

			..     "label[0.50,0.50;"..minetest.formspec_escape(core.colorize("#00FFFF", modTradeLands.translate("LAND PROTECTION WILL"))).."]"
			
			..  "textarea[1.00,2.00;9.0,1.0;;"..minetest.formspec_escape(core.colorize("#FFFF00", modTradeLands.translate("CURRENT PROTECTION")..":"))..";"..minetest.formspec_escape(strValidate.." "..strRest).."]"
			..  "textarea[1.00,3.00;9.0,1.0;;"..minetest.formspec_escape(core.colorize("#00FFFF", modTradeLands.translate("NEW PROTECTION DATE")..":"))..";"..minetest.formspec_escape(strNewValidate).."]"
			..  "textarea[1.00,4.00;9.0,1.0;;"..minetest.formspec_escape(core.colorize("#00FF00", modTradeLands.translate("BANK BALANCE")..":"))..";"..minetest.formspec_escape(msgBalance).."]"
			..  "textarea[1.00,5.00;9.0,2.0;;"..minetest.formspec_escape(core.colorize("#FF8800", modTradeLands.translate("NEW PROTECTION COST")..":"))..";"..minetest.formspec_escape(msgPriceOfProtection).."]"
			..  "textarea[1.00,7.00;9.0,2.5;;"..minetest.formspec_escape(core.colorize("#00FFFF", modTradeLands.translate("DETAILS")..":"))..";"..minetest.formspec_escape(msgDetails).."]"
			.."button_exit[3.00,9.50;3.00,1.00;btnPay2;"..minetest.formspec_escape(modTradeLands.translate("PAY")).."]"
			.."button_exit[6.00,9.50;3.00,1.00;btnCancel;"..minetest.formspec_escape(modTradeLands.translate("Cancel")).."]"
			return formspec
		end --fim de if modMinerTrade ~= "" then
	end
	
	modTradeLands.doPay2 = function(playername) --Faz o pagamento do terreno
		if modMinerTrade.isExistAcount("THE GOVERNMENT") then
			if type(modTradeLands.formPlayer)~="nil" 
				and type(modTradeLands.formPlayer[playername])~="nil" 
				and type(modTradeLands.formPlayer[playername].selPos)~="nil" 
			then
				local selPos = modTradeLands.formPlayer[playername].selPos
				local restDays = modTradeLands.getDaysRest(selPos)
				if restDays <= modTradeLands.protected_days then --Verifica se o jogador ja tem pago adiantado
					local balance = modMinerTrade.getBalance(playername)
					local price = modTradeLands.minercash_price
					if balance >= price then
						local txtLandName = modTradeLands.getLandName(selPos)
						--modMinerTrade.getBalance = function(playername)
						
						modTradeLands.setOwnerName(selPos, playername)
						modTradeLands.setIfDamageInteract(selPos, true)
						modTradeLands.setPvpType(selPos, modTradeLands.default_pvp)
						modTradeLands.setValidate(selPos, modTradeLands.getNewValidate(selPos))
						modTradeLands.doSave()
						
						modMinerTrade.addBalance(playername, 0 - price)
						modMinerTrade.addStatement(
							playername, 0 - price, 
							modTradeLands.translate(
								"Protected the field (%s)!"
							):format(txtLandName)
						)
						modMinerTrade.addBalance("THE GOVERNMENT", price)
						--[[  --NÃO REGISTRA ENTRADAS NO EXTRATO GOVERNAMENTAL
						modMinerTrade.addStatement(
							"THE GOVERNMENT", price, 
							modTradeLands.translate(
								"The player '%s' protected (%s)!"
							):format(playername, txtLandName)
						)
						--[[  ]]
						modMinerTrade.doBankSave()
						
						minetest.chat_send_all(
							core.colorize("#00FF00", "[TRADELANDS] ")
							.. modTradeLands.translate(
								"The player '%s' protected the field (%s)!"
							):format(
								playername, txtLandName
							) 
						)
						modTradeLands.doShowLand(playername)
						modTradeLands.doSoundProtector()
					else
						local msg = modTradeLands.translate("You don't have enough minercash in your bank account to pay for land protection.")
						minetest.show_formspec(
							playername, "frmTradelands", 
							modTradeLands.getFormPay2(
								selPos, playername,
								core.colorize("#FFFF00", 
									msg
								)
							)
						)
						minetest.chat_send_player(playername, 
							core.colorize("#FFFF00", "[TRADELANDS:ALERT]")
							.." "..msg
						)
						modMinerTrade.doSoundPlayer(playername, "sfx_failure", 5)
					end
				else
					local msg = modTradeLands.translate("You can pay an advance license fee only once!")
					minetest.chat_send_player(playername, 
						core.colorize("#FFFF00", "[TRADELANDS:ALERT]")
						.." "..msg
					)
					modMinerTrade.doSoundPlayer(playername, "sfx_failure", 5)
					minetest.show_formspec(
						playername, "frmTradelands", 
						modTradeLands.getFormPay2(
							selPos, playername,
							core.colorize("#FFFF00", 
								msg
							)
						)
					)
				end
				modTradeLands.giveChange(playername)
			end
		else
			minetest.log(
			   'error', (
			      "[TRADELANDS:ERROR] %s >>> There is no '%s' bank account!"
				):format(
					"modTradeLands.doPay2('THE GOVERNMENT')", 
					"THE GOVERNMENT"
				)
			)
		end
	end
end


