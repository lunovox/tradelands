


modTradeLands.autochange_landrush = true --If the admin wants the permanent protections of the mod landrush to be automatically replaced by the temporary protections of the mod tradelands as the player walks by the map.

modTradeLands.time_showarea = 60 --Time in secounds to to show the limit of land size.

modTradeLands.areaSize = {
	side = 16, --16x16 size of land
	high = 200, --100 to up, and 100 to down
}

modTradeLands.protected_days = 15 --The addition of time on the protection of the land. If the value is 0 (zero) then the protection will be 100 real years (same that permanent).

modTradeLands.minercash_price = 9 --Price in Minercash pay in Bank Account per 'protected_days'.

modTradeLands.damage_interact = 6 --The amount of damage that the player will receive if it forces interact with the terrain. (6 x 0.5 = 3HP)

modTradeLands.auto_flip = true --Rotate the player on interact with a protected land of other player.

modTradeLands.ChestLocked = {
   replaceProtection = true 
}

