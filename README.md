![screeshot]

# [TRADELANDS][repository_tradelands]

Protection of land on payment of periodic rate. The Minetest player can set permissions of PVP for his terrain. Need to craft a land protection charter.

Badge: [![ContentDB_en_icon]][ContentDB_en_link] [![DownloadsCDB_en_icon]][DownloadsCDB_en_link]

-----------------------------------------

## **Dependencies:**

* default : Minetest Game Included
* dye : Minetest Game Included
* door : Minetest Game Included
* [minertrade][content_minertrade] : Monetary money mod.

## **Optional Dependencies:**

* [intllib][content_intllib] : Facilitates the translation of several other mods into your native language, or other languages.
* [computing][repository_computing] : Adding integration with 'computing' mod applications.
* [eurn][repository_eurn] : Allows the elected president to access protected land, and access the money collected from land protection to use to hire players to build public works.

-----------------------------------------

## **License:**

* [![license_code_icon]][license_code_link]
* [![license_media_icon]][license_media_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

-----------------------------------------

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:mastodon.social/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https://libreplanet.org/wiki/User:Lunovox)

-----------------------------------------

## **Translate this mod to Your Language:**

See more details in file: [locale/README.md]

-----------------------------------------

[config.lua]:https://gitlab.com/lunovox/tradelands/-/blob/master/config.lua?ref_type=heads
[locale/README.md]:https://gitlab.com/lunovox/tradelands/-/blob/master/locale/README.md?ref_type=heads
[screeshot]:https://gitlab.com/lunovox/tradelands/-/raw/master/screenshot.png

[repository_computing]:https://gitlab.com/lunovox/computing/
[repository_eurn]:https://gitlab.com/lunovox/e-urn/
[repository_tradelands]:https://gitlab.com/lunovox/tradelands/
[content_intllib]:https://content.luanti.org/packages/kaeza/intllib/
[content_minertrade]:https://content.luanti.org/packages/Lunovox/minertrade/

[ContentDB_en_icon]:https://content.luanti.org/packages/Lunovox/tradelands/shields/title/
[ContentDB_en_link]:https://content.luanti.org/packages/Lunovox/tradelands/
[DownloadsCDB_en_icon]:https://content.luanti.org/packages/Lunovox/tradelands/shields/downloads/
[DownloadsCDB_en_link]:https://gitlab.com/lunovox/tradelands/-/tags

[license_code_icon]:https://img.shields.io/static/v1?label=LICENSE%20CODE&message=GNU%20AGPL%20v3.0&color=yellow
[license_code_link]:https://gitlab.com/lunovox/tradelands/-/raw/master/LICENSE_CODE
[license_media_icon]:https://img.shields.io/static/v1?label=LICENSE%20MEDIA&message=CC%20BY-SA-4.0&color=yellow
[license_media_link]:https://gitlab.com/lunovox/tradelands/-/raw/master/LICENSE_MEDIA
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License




