local ngettext


if type(minetest.get_translator) ~= "nil" 
   and type(minetest.get_current_modname) ~= "nil" 
   --and type(minetest.get_modpath(minetest.get_current_modname())) ~= "nil" 
   and type(minetest.get_translator(minetest.get_current_modname())) ~= "nil"
then
	modTradeLands.translate = minetest.get_translator(minetest.get_current_modname())
elseif minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		modTradeLands.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		modTradeLands.translate = intllib.Getter()
	end
else
	modTradeLands.translate = function(s) return s end
end
